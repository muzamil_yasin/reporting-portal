package com.asterisk.reporting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReportingPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReportingPortalApplication.class, args);
	}

}
